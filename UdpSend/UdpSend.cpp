#include "UdpSend.h"
#include <iostream>


UdpSend::UdpSend(QWidget* parent)
    : QWidget(parent),
      m_pUdpSocket(nullptr)
{
    ui.setupUi(this);
    m_pUdpSocket = new  QUdpSocket(this);//创建socket对象
    connect(ui.sendButton, &QPushButton::clicked, this, &UdpSend::dealInfo);
}

void UdpSend::dealInfo()
{
    if (showStatus())
    {
        setEmployInfo(ui.numEdit->text().toStdString(),
            ui.nameEdit->text().toStdString(),
            ui.ageEdit->text().toShort(),
            ui.positionEdit->text().toStdString(),
            ui.salaryEdit->text().toShort());
        sendData();
        ui.resultLabel->setText(QString(QString::fromLocal8Bit("发送成功")));
    }
    else
    {
        ui.resultLabel->setText(QString(QString::fromLocal8Bit("发送失败!!!")));
    }
}

void UdpSend::setEmployInfo(std::string strNum, std::string strName, int iAge, std::string strPosition, int iSalary)
{
    m_employee.m_strNum = strNum;
    m_employee.m_strName = strName;
    m_employee.m_iAge = iAge;
    m_employee.m_strPosition = strPosition;
    m_employee.m_iSalary = iSalary;

}

bool UdpSend::showStatus()
{
    if (ui.numEdit->text().isEmpty()
        || ui.nameEdit->text().isEmpty()
        || ui.ageEdit->text().isEmpty()
        || ui.positionEdit->text().isEmpty()
        || ui.salaryEdit->text().isEmpty())
    {
        return false;
    }

    return true;
}

bool UdpSend::sendData()
{
    char pAge[10];
    sprintf(pAge, "%d", m_employee.m_iAge);

    char pSalary[100];
    sprintf(pSalary, "%d", m_employee.m_iSalary);

	QByteArray strData;
	strData.push_back(m_employee.m_strNum.c_str());
    strData.push_back(" ");
	strData.push_back(m_employee.m_strName.c_str()); 
    strData.push_back(" ");
	strData.push_back(pAge);
    strData.push_back(" ");
	strData.push_back(m_employee.m_strPosition.c_str());
    strData.push_back(" ");
	strData.push_back(pSalary);
    QString qsrtText = strData;
    //std::string strTet = qsrtText.toStdString();
    
    qDebug() << qsrtText;
    m_pUdpSocket->writeDatagram(strData, QHostAddress::Broadcast, 88888);
    return true;
}
