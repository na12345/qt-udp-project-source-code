/********************************************************************************
** Form generated from reading UI file 'UdpSend.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UDPSEND_H
#define UI_UDPSEND_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UdpSendClass
{
public:

    void setupUi(QWidget *UdpSendClass)
    {
        if (UdpSendClass->objectName().isEmpty())
            UdpSendClass->setObjectName(QStringLiteral("UdpSendClass"));
        UdpSendClass->resize(600, 400);

        retranslateUi(UdpSendClass);

        QMetaObject::connectSlotsByName(UdpSendClass);
    } // setupUi

    void retranslateUi(QWidget *UdpSendClass)
    {
        UdpSendClass->setWindowTitle(QApplication::translate("UdpSendClass", "UdpSend", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UdpSendClass: public Ui_UdpSendClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UDPSEND_H
