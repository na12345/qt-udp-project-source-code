#ifndef UDP_SEND_H
#define UDP_SEND_H

#include <QtWidgets/QWidget>
#include <QUdpSocket>
#include <QByteArray>
#include <QString>
#include <QLabel>
#include <QPushButton>

#include "ui_UdpSend.h"

struct Employee
{
    std::string m_strNum;
    std::string m_strName;
    int m_iAge;
    std::string m_strPosition;
    int m_iSalary;
};


class UdpSend : public QWidget
{
    Q_OBJECT

public:
    UdpSend(QWidget *parent = Q_NULLPTR);

public slots:
    void dealInfo();
    
private:
    Ui::UdpSendClass ui;
    Employee m_employee;
    QUdpSocket* m_pUdpSocket;

private:
    void setEmployInfo(std::string strNum, std::string strName, int iAge, std::string strPosition, int iSalary);
    bool showStatus();//对发送的数据进行判断（是否存在空字段发送）
    bool sendData();
};
#endif