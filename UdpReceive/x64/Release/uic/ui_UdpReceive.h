/********************************************************************************
** Form generated from reading UI file 'UdpReceive.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UDPRECEIVE_H
#define UI_UDPRECEIVE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UdpReceiveClass
{
public:

    void setupUi(QWidget *UdpReceiveClass)
    {
        if (UdpReceiveClass->objectName().isEmpty())
            UdpReceiveClass->setObjectName(QStringLiteral("UdpReceiveClass"));
        UdpReceiveClass->resize(600, 400);

        retranslateUi(UdpReceiveClass);

        QMetaObject::connectSlotsByName(UdpReceiveClass);
    } // setupUi

    void retranslateUi(QWidget *UdpReceiveClass)
    {
        UdpReceiveClass->setWindowTitle(QApplication::translate("UdpReceiveClass", "UdpReceive", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UdpReceiveClass: public Ui_UdpReceiveClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UDPRECEIVE_H
