#ifndef UDP_RECEIVE_H
#define UDP_RECEIVE_H


#include <QtWidgets/QWidget>
#include <QTableWidget>
#include <QUdpSocket>
#include <QByteArray>
#include <QDebug>
#include <QString>
#include <QPushButton>

#include "ui_UdpReceive.h"

class UdpReceive : public QWidget
{
    Q_OBJECT

public:
    UdpReceive(QWidget *parent = Q_NULLPTR);

private:
    Ui::UdpReceiveClass ui;
    QUdpSocket* m_pUdpSocket;

private slots:
    void recevData();
    //void addItem();

private:
    void showData(const QByteArray& data);//将接收的数据显示到表格中
};


#endif