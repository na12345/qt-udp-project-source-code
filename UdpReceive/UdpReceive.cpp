#include "UdpReceive.h"

UdpReceive::UdpReceive(QWidget *parent)
    : QWidget(parent),
      m_pUdpSocket(nullptr)
{
    ui.setupUi(this);
    m_pUdpSocket = new QUdpSocket(this);
    m_pUdpSocket->bind(88888, QUdpSocket::ShareAddress);

    connect(m_pUdpSocket, &QUdpSocket::readyRead, this, &UdpReceive::recevData);
    //connect(ui.insetButton, &QPushButton::clicked, this, &UdpReceive::addItem);
    

}

void UdpReceive::recevData()
{
	QByteArray Datagram;
	while (m_pUdpSocket->hasPendingDatagrams())
	{
        Datagram.resize(m_pUdpSocket->pendingDatagramSize());
        m_pUdpSocket->readDatagram(Datagram.data(), Datagram.size());
        //Datagram.at(0)
        QString strText = Datagram;
        qDebug() << strText;
        showData(Datagram);
        //qDebug() <<QString(QString::fromLocal8Bit(Datagram.toStdString().c_str()));
	}
}


void UdpReceive::showData(const QByteArray& Datagram)
{
    int irowCount = ui.tableWidget->rowCount();

    QTableWidgetItem* Item0 = new QTableWidgetItem();
    QTableWidgetItem* Item1 = new QTableWidgetItem();
    QTableWidgetItem* Item2 = new QTableWidgetItem();
    QTableWidgetItem* Item3 = new QTableWidgetItem();
    QTableWidgetItem* Item4 = new QTableWidgetItem();
    ui.tableWidget->insertRow(irowCount);

    QString strData = Datagram;
    QStringList list = strData.split(" ");
    Item0->setText(list[0]);
    Item1->setText(list[1]);
    Item2->setText(list[2]);
    Item3->setText(list[3]);
    Item4->setText(list[4]);

    ui.tableWidget->setItem(irowCount, 0, Item0);
    ui.tableWidget->setItem(irowCount, 1, Item1);
    ui.tableWidget->setItem(irowCount, 2, Item2);
    ui.tableWidget->setItem(irowCount, 3, Item3);
    ui.tableWidget->setItem(irowCount, 4, Item4);

    
}
